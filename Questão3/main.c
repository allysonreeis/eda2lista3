#include <stdio.h>
#include <stdlib.h>
#include "mergeSort.h"

int main () {
	int vetor[] = {5, 3, 65, 23, 10, 0, 1, 9, 8, 7, 2};
	int i;
	int tam = sizeof(vetor) / sizeof (int);
	mergeSort(vetor, tam);

	for (i = 0; i < tam; i++) {
		printf("%d ", vetor[i]);
	}

	putchar('\n');
	return 0;
}
