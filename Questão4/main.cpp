#include<stdio.h>
#include<stdlib.h>
#include<RadixSort.h>

#define TAMANHO 10

int main(){

	int vetor[TAMANHO];
	int i;
	for(i=0; i < TAMANHO; i++){
		vetor[i] = (rand() % 1000000);
		printf("%d, ", vetor[i]);
	}
	printf("\n\n");
	RadixSort(vetor, TAMANHO);

	for(i=0; i < TAMANHO; i++)
		printf("%d, ", vetor[i]);
	printf("\n");
	
	return 0;
}
